import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DrunkStoreGalleryComponent } from './components/drunk-store-gallery/drunk-store-gallery.component';
import { DrunkStoreDetailComponent } from './components/drunk-store-gallery/drunk-store-detail/drunk-store-detail.component';
import { NewAlcoholicDrinkComponent } from './components/new-alcoholic-drink/new-alcoholic-drink.component';
import { AlcoholicDrinksService } from './components/services/alcoholic-drinks.service';

@NgModule({
  declarations: [
    AppComponent,
    DrunkStoreGalleryComponent,
    DrunkStoreDetailComponent,
    NewAlcoholicDrinkComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [AlcoholicDrinksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
