import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAlcoholicDrinkComponent } from './new-alcoholic-drink.component';

describe('NewAlcoholicDrinkComponent', () => {
  let component: NewAlcoholicDrinkComponent;
  let fixture: ComponentFixture<NewAlcoholicDrinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewAlcoholicDrinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAlcoholicDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
