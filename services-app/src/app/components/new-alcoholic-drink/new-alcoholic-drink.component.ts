import { AlcoholicDrinksService } from './../services/alcoholic-drinks.service';
import { Component, OnInit } from '@angular/core';
import {  FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IalcoholicDrink } from '../models/ialcoholic-drink';

@Component({
  selector: 'app-new-alcoholic-drink',
  templateUrl: './new-alcoholic-drink.component.html',
  styleUrls: ['./new-alcoholic-drink.component.scss']
})
export class NewAlcoholicDrinkComponent implements OnInit {
  public alcoholicDrinkForm: FormGroup | any = null;
  // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;


  constructor(private formBuilder: FormBuilder, private alcoholicDrinksService: AlcoholicDrinksService) {

    this.alcoholicDrinkForm  = this.formBuilder.group({

      name: ['', Validators.required],
      type: ['', Validators.required],
      image: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required],

    });
  }

  ngOnInit(): void {
  }

  public onSubmit(): void{
    this.submitted = true;

    if (this.alcoholicDrinkForm.valid){

      const alcoholicDrink: IalcoholicDrink = {
        name: this.alcoholicDrinkForm.get('name').value,
        type: this.alcoholicDrinkForm.get('type').value,
        image: this.alcoholicDrinkForm.get('image').value,
        price: this.alcoholicDrinkForm.get('price').value,
        description: this.alcoholicDrinkForm.get('description').value,
      };
      console.log(alcoholicDrink);
      debugger;
      this.alcoholicDrinksService.setAlcoholicDrink(alcoholicDrink);

      // Reseteamos los campos del formulario:
      this.alcoholicDrinkForm.reset();
      this.submitted = false;
    }
  }
}
