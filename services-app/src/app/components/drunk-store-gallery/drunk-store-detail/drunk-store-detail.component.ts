import { Component, Input, OnInit } from '@angular/core';
import { IalcoholicDrink } from './../../models/ialcoholic-drink';

@Component({
  selector: 'app-drunk-store-detail',
  templateUrl: './drunk-store-detail.component.html',
  styleUrls: ['./drunk-store-detail.component.scss']
})
export class DrunkStoreDetailComponent implements OnInit {
  @Input() myDrinks: IalcoholicDrink | any = {};
  constructor() { }

  ngOnInit(): void {
  }

}
