import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrunkStoreDetailComponent } from './drunk-store-detail.component';

describe('DrunkStoreDetailComponent', () => {
  let component: DrunkStoreDetailComponent;
  let fixture: ComponentFixture<DrunkStoreDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrunkStoreDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrunkStoreDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
