import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrunkStoreGalleryComponent } from './drunk-store-gallery.component';

describe('DrunkStoreGalleryComponent', () => {
  let component: DrunkStoreGalleryComponent;
  let fixture: ComponentFixture<DrunkStoreGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrunkStoreGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrunkStoreGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
