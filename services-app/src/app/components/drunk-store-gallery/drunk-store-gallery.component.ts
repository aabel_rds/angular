import { AlcoholicDrinksService } from './../services/alcoholic-drinks.service';
import { IalcoholicDrink } from './../models/ialcoholic-drink';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-drunk-store-gallery',
  templateUrl: './drunk-store-gallery.component.html',
  styleUrls: ['./drunk-store-gallery.component.scss']
})
export class DrunkStoreGalleryComponent implements OnInit {

  public myDrinks: IalcoholicDrink[] | null = null;
  constructor(private alcoholicDrinkService: AlcoholicDrinksService) {}

  ngOnInit(): void {

    this.myDrinks = this.alcoholicDrinkService.getAlcoholicDrinks();
  }

}
