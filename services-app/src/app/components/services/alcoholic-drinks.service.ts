import { IalcoholicDrink } from './../models/ialcoholic-drink';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlcoholicDrinksService {

  private drink: IalcoholicDrink[];

  constructor() {
    this.drink = [{

      name: 'Ron Extra Añejo Cacique 500 - 700 ml',
      type: 'RON',
      image: 'https://i.pinimg.com/originals/07/54/b8/0754b820b98d45c54b4457a04a731b43.jpg',
      price: 23,
      description: 'Este producto Ron Cacique 500 viene en un pack con posavasos de regalo de 700 ml. Para compartir en cualquier occasion. Ron Anejo Cacique 500 esta producido exclusivamente en la Hacienda Saruro, en el Estado Lara, Venezuela, donde los mas puros manantiales de agua y fertiles plantaciones de cana, ofrece los mejores ingredientes, 100% naturales.',
    }];
  }

  getAlcoholicDrinks(): IalcoholicDrink[] {

    return this.drink;
  }

  setAlcoholicDrink(alcoholicDrink: IalcoholicDrink): void {
    debugger;
    this.drink.push(alcoholicDrink);
  }
}
