import { TestBed } from '@angular/core/testing';

import { AlcoholicDrinksService } from './alcoholic-drinks.service';

describe('AlcoholicDrinksService', () => {
  let service: AlcoholicDrinksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlcoholicDrinksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
