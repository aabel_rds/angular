export interface IalcoholicDrink {

  name: string;
  type: string;
  description: string;
  price: number;
  image: string;
}

