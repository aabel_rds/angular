import { ImovieVideoId } from './../../models/imovie-video-id';
import { ImovieSearch } from '../../models/imovie-search';
import { URL } from './../../key/key';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class MoviesService {
    // Crear form para poder pasarle a la URl el "name"
  public name: string = "";
  private movieSearchUrl: string = URL.movieSearchUrl;
  private movieTrending: string = URL.movieTrending;
  private search: string = "";
  private movieKey: string = "";
  private movieId: number = 0;
  private movieVideo: string = URL.youTube;



  constructor(private http: HttpClient) { /* empty */ }

  // ver por qué no me deja tipar la petición
  public getMovie(movieRequest: string ): Observable <ImovieSearch> {

    this.name = movieRequest;
    console.log("En el servicio",movieRequest);

    if(this.name === ""){

      this.search = this.movieTrending;
    }else{

      this.search = this.movieSearchUrl+this.name;
    }
    return this.http.get(this.search).pipe(map((response: any) => {

        if (!response) {
          throw new Error('Value expected!');
        } else {
          console.log('All request (Service)', response);
          return response;
        }
    }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public getMovieID(movieID: number ): Observable <ImovieVideoId> {
    console.log(this.movieId);
    return this.http.get(URL.movieId_1+movieID+URL.movieId_2).pipe(map((response: any) => {

        if (!response) {
          throw new Error('Value expected!');
        } else {
          console.log('MovieID Object', response);
          return response;
        }
    }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

}
