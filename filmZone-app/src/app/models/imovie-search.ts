export interface ImovieSearch {
  page: number;
  results: Result[];
  total_pages: number;
  total_results: number;

}

export interface Result {

    adult: boolean;
    backdrop_path: string | null;
    genre_ids: number[];
    id: number;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path: string | null;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
    release_date: string;
}


export interface Movies{
  adult: boolean;
  backdrop_path: string | null;
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string | null;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
  release_date: string;
}
