import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { YouTubePlayerModule } from "@angular/youtube-player";
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { FormSearchComponentComponent } from './home-component/form-search-component/form-search-component.component';
import { MovieGalleryComponentComponent } from './home-component/movie-gallery-component/movie-gallery-component.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalDetailComponentComponent } from './home-component/modal-detail-component/modal-detail-component.component';


@NgModule({
  declarations: [HomeComponent, FormSearchComponentComponent, MovieGalleryComponentComponent, ModalDetailComponentComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    YouTubePlayerModule
  ]
})
export class HomeModule { }
