import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-search-component',
  templateUrl: './form-search-component.component.html',
  styleUrls: ['./form-search-component.component.scss']
})
export class FormSearchComponentComponent implements OnInit {
  public searchForm: FormGroup | any;
  public submitted: boolean = false;

  @Output() private movieRequest: EventEmitter<string> = new EventEmitter<string>();

  search: string = "";

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.searchMovieForm();
  }
  public searchMovieForm(){

    this.searchForm = this.formBuilder.group({

      //This is a simple form to search movies.
      search: ['', [Validators.minLength(4) ]]
    })
  }

  public onSubmit(): void{

    this.submitted = true;

    if(this.searchForm.valid){
      this.search = this.searchForm.get('search').value;
      this.movieRequest.emit(this.search);
      console.log(this.search);
      console.log(this.movieRequest);
      this.searchForm.reset();
    };
  }
}
