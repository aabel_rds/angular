import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSearchComponentComponent } from './form-search-component.component';

describe('FormSearchComponentComponent', () => {
  let component: FormSearchComponentComponent;
  let fixture: ComponentFixture<FormSearchComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSearchComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSearchComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
