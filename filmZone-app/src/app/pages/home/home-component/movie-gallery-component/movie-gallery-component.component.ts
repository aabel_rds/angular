import { element } from 'protractor';
import { map } from 'rxjs/operators';
import { Result } from './../../../../models/imovie-search';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Target } from '@angular/compiler';

@Component({
  selector: 'app-movie-gallery-component',
  templateUrl: './movie-gallery-component.component.html',
  styleUrls: ['./movie-gallery-component.component.scss']
})
export class MovieGalleryComponentComponent implements OnInit {
  // Tipar
  @Input() public showModal: boolean = false;
  @Input() movie: any;
  @Input() imgUrl: string = "";
  @Output() movieDetail = new EventEmitter<number>();

  constructor() { }
  public movies: any = [];
  public movieId: string = "";
  ngOnInit(): void {
    // console.log('MOVIE-GALLERY', this.movies);
  }

  getImgDetails(event: null | any ){
    if(event !== null){
      this.movies =this.movie;
      this.movieId = event.id;
      // this.movies= this.movies.push(event.id);



      this.movieDetail.emit(this.movies);
      console.log('eventoGaleria', this.movies);


    }
  }

}
