import { element } from 'protractor';
import { map } from 'rxjs/operators';
import { MoviesService } from './../../../services/movies/movies.service';
import { Movies, Result } from './../../../models/imovie-search';
import { Component, Input, OnInit, ɵConsole} from '@angular/core';
import { Location } from '@angular/common';
import { URL } from '../../../key/key';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @Input() public showModal: boolean = false;

  public search: Result[] = [];
  public imgUrl = URL.imgUrl;
  public movieRequest: string = "";
  public movieKey: string = "";
  public UrlMovieVideo: string = "";
  public movieDetail: any = [];



  constructor(private movieService: MoviesService, private location: Location) { /* empty */}

  ngOnInit(): void {
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
    this.getMovies();

  }


  public getMovies(): void{
    console.log('En el padre', this.movieRequest);
    this.movieService.getMovie(this.movieRequest).subscribe((result) => {

      this.search = result.results;
      console.log('Results(Home)', this.search);
    },
    (err) => {
      console.error(err.message);
    }
    );
  }
  public goBack(): void{
    this.location.back();
  }
  public getSearch(movie: string): void{

    this.movieRequest = movie;
    return this.getMovies();
  }

  public getMovieId(movieID: any): void{
    this.movieDetail = movieID;
    console.log('MOVIE DETAILLLL :)',this.movieDetail);
    console.log('En el padre', movieID);
    movieID = movieID.id;

    this.movieService.getMovieID(movieID).subscribe((result) => {
      result.results.find(element => this.movieKey = element.key)
      console.log(typeof(this.movieKey));
      console.log(this.search);

      this.UrlMovieVideo = URL.youTube + this.movieKey;
      console.log('MOVEID',result.results);

      // result.results.forEach(element => {

      //   this.movieDetail.push(element.key);
      // });



    },
    (err) => {
      console.error(err.message);
    }
    );
    this.showModal = true;
  }

}
