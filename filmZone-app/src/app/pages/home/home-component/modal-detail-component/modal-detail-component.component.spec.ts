import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetailComponentComponent } from './modal-detail-component.component';

describe('ModalDetailComponentComponent', () => {
  let component: ModalDetailComponentComponent;
  let fixture: ComponentFixture<ModalDetailComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDetailComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetailComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
