import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal-detail-component',
  templateUrl: './modal-detail-component.component.html',
  styleUrls: ['./modal-detail-component.component.scss']
})
export class ModalDetailComponentComponent implements OnInit {
  @Input() public showModal: boolean = false;
  @Output() private showModalChange = new EventEmitter<boolean>();

  constructor() { }


  public closeShowModal(): void {

    this.showModalChange.emit(!this.showModal);
  }
  ngOnInit(): void {
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
  }

}
