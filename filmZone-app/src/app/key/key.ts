export const URL ={

  apiKey: '2059be036720300deff46b1e4723eb5f',

  imgUrl: 'https://image.tmdb.org/t/p/w500',

  movieSearchUrl:  `https://api.themoviedb.org/3/search/movie?api_key=2059be036720300deff46b1e4723eb5f&page=1&language=es-ESP&append_to_response=images&include_image_language=en,null&query=`,

  movieTrending: `https://api.themoviedb.org/3/trending/movie/day?api_key=2059be036720300deff46b1e4723eb5f`,

  movieId_1: `https://api.themoviedb.org/3/movie/`,
  movieId_2: `/videos?api_key=2059be036720300deff46b1e4723eb5f&language=en-US`,

  youTube: `www.youtube.com/embed/`
}
